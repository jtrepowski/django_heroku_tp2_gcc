from django.http import HttpRequest
from django.test import SimpleTestCase
from django.urls import reverse

from . import views


class LoginPageTests(SimpleTestCase):

    def test_home_page_status_code(self):
        response = self.client.get('/')
        self.assertEquals(response.status_code, 200)

    def test_view_uses_correct_template(self):
        response = self.client.get('admin/')
        self.assertEquals(response.status_code, 404)
