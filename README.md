**TP GCC 2019**

El siguiente proyecto es realizado para la materia de gestión de centros de cómputos, 
que consiste en desplegar una aplicación utilizando la herramienta HEROKU, y aplicando el proceso CI/CD con GitLab.

**Manejo de ambientes: para la prueba se consideran 3 ambientes**

Desarrollo, homologación y producción.
En el ambiente de desarrollo se considera la instalación de los requerimientos  y dependencias junto a las versiones las herramientas utilizadas.
En el ambiente de homologación se realiza la ejecución de las pruebas unitarias del sistema junto con la verificación de las mismas.
En el ambiente de producción, se realiza el despliegue de la aplicación sobre la plataforma HEROKU, en dos etapas “staging and production”, solo en caso de qué se hayan pasado de forma satisfactoria las siguientes etapas.
 
El .gitlab-ci.yml es el archivo qué define la estructura y el orden de los  pipelines, y esté fichero consta de diferentes partes:
Parámetros de configuración utilizados:
 
* [ ] image: Usa imágenes docker en esté caso python
* [ ] script: los comandos qué se van a ejecutar en cada stage o etapa
* [ ] services: Utiliza los servicios de la base de datos postgresql.
* [ ] stages: existen 3 ambientes desarrollo, producción y homologacion.
* [ ] variables: se define la variable de la base de datos qué se utiliza, en esté caso postgres.
* [ ] La aplicación se deploys en HEROKU, tanto en desarrollo cómo en producción .

