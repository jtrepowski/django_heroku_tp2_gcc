Django==2.0
gunicorn==19.7.1
pytz==2017.3
psycopg2-binary==2.7.4
setuptools==40.6.3
django-heroku
